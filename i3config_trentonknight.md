# My slowly growing i3 configurations
* [https://i3wm.org/docs/](https://i3wm.org/docs/)
* [https://github.com/vivien/i3blocks](https://github.com/vivien/i3blocks)

Set gaps, remove boarders and set background wallpaper.

```bash
gaps outer 10
gaps inner 20
default_border pixel 0
default_floating_border none

exec feh --no-fehbg --bg-scale '/home/trentonknight/Pictures/background.jpg'
```
# Betterlockscreen setup

```bash
git clone https://aur.archlinux.org/i3lock-color.git
cd i3lock-color
makepkg -si
git clone https://aur.archlinux.org/betterlockscreen-git.git
cd betterlockscreen-git
makepkg -si
sudo pacman -Sy xss-lock
```

## Append to '.config/i3/config`

```bash
exec --no-startup-id xss-lock --transfer-sleep-lock -- betterlockscreen -l 
bindsym $mod+shift+x exec betterlockscreen -l 
```
### cache image for betterlock
```bash
betterlockscreen -u ~/Pictures/Forests.png
```
# Set terminal

```bash
bindsym $mod+Return exec alacritty
```

# i3blocks

Add i3blocks to the i3 config `~/.config/i3/config`

```bash

bar {
        status_command i3blocks
	colors {
	background #323232
	statusline #d65219
	focused_workspace #d65219 #d65219 #000000
	inactive_workspace #d65219 #323232 #d65219
    }
    }
    client.focused #BC3908 #BC3908 #333333 #2e9ef4 #285577
    client.unfocused #333333 #222222 #ff6600 #292d2e #222222

```
## i3blocks config `~/.config/i3blocks/config/`

```bash
# Reboot computer
[reboot]
full_text=systemctl reboot
command=systemctl reboot

# Restart i3 on click
[restart i3]
full_text=restart i3
command=i3-msg -q restart
#interval=0

# Update time every 5 seconds
[time]
command=date +%T
interval=5





